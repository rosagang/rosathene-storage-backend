const {Storage} = require('@google-cloud/storage');

const cloudstorage = new Storage({keyFilename: 'RosAthene-309377f4b126.json', projectId: 'rosathene'})
const bucketName = 'rosathene-picture-bucket'; 
const bucket = cloudstorage.bucket(bucketName);

function uploadFileToGoogleCloud(filename, apicb) {
    const options = {
        gzip: true,
        metadata: {
            cacheControl: 'public, max-age=31536000'
        }
    }
    return bucket.upload(filename, options, apicb);
}

module.exports = uploadFileToGoogleCloud;