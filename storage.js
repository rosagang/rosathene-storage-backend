require("dotenv").config();
const express = require("express");
const multer = require("multer");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const uploadFileToGoogleCloud = require("./google-cloud");
const crypto = require("crypto");
const { storeToDatabase, getAreaPolys } = require("./database");
const { authenticateJWT } = require("./jwt");
var fs = require("fs");
const { Polys } = require("./models/upload");
const pino = require("pino");
const expressPino = require("express-pino-logger");

const logger = pino({ level: process.env.LOG_LEVEL || "info" });
const expressLogger = expressPino({ logger });

const corsOptions = {
  origin: (origin, cb) => {
    cb(null, process.env.FRONT_END_PORT);
  },
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    console.log("validate folder");
    if (!fs.existsSync("./uploads")) {
      fs.mkdirSync("./uploads");
    }
    cb(null, "./uploads");
  },
  filename: (req, file, cb) => {
    let newName =
      Date.now().toString() + crypto.randomBytes(8).toString("hex") + ".png";
    cb(null, newName);
  },
});

const fileFilter = (req, file, cb) => {
  cb(null, true);
};

const upload = multer({ storage: storage, fileFilter: fileFilter });

const app = express();

const PORT = process.env.PORT || 4000;

const location = {
  lan: 0,
  lon: 0,
};

app.use(expressLogger);
app.use(cors(corsOptions));

app.post(
  "/upload",
  authenticateJWT,
  upload.array("picture", 10),
  (req, res, next) => {
    if (!req.user) {
      return res
        .json({ message: "need to be logged in as admin to upload" })
        .sendStatus(403);
    }
    if (req.user.role !== "admin") {
      return res
        .json({ message: "need to be logged in as admin to upload" })
        .sendStatus(403);
    }
    if (!req.files) {
      return res.json({ message: "no files" }).sendStatus(403);
    }
    logger.debug('req.body.logs: %o', req.body.logs);
    logger.debug(req.files);
    const polys = JSON.parse(req.body.polys);
    const date = req.body.date;
    let promises = [];

    let apicb = (err, file, apiRes) => {
      logger.debug("file %s uploaded to google cloud", file.name);
      if(err)
        logger.error(err);
    }
    req.files.forEach(
      (f) => { promises.push(uploadFileToGoogleCloud(f.path, apicb));
    });
    Promise.all(promises)
      .then(_ => {
        storeToDatabase(req.files, req.user.username, polys, date);
      })
      .then((_) => {
        res.status(200).json({ message: "upload worked" }).send();
      });
  }
);

app.get("/api/uploads/:user", authenticateJWT, (req, res) => {
  if (!req.user) {
    return res
      .json({ message: "need to be logged in to access uploads" })
      .status(403);
  }
  const username = req.params.user;
  if (req.user.username !== username) {
    return res.json({ message: "cant access data for this user" }).status(403);
  }
});

app.get("/api/areapolys/", async (req, res) => {
  const polys = await getAreaPolys();
  console.log("polys: ", polys);
  res.json({ polys: polys });
});

app.listen(PORT, () => console.log("listening on port " + PORT));
