const mongoose = require("mongoose");
const { ImageUpload, Polys } = require("./models/upload");

const psw = process.env.DB_KEY;
const dbname = "uploads";

const dbpath = `mongodb+srv://rosathene:${psw}@cluster0.scpfj.mongodb.net/${dbname}?retryWrites=true&w=majority`;

mongoose.connect(dbpath, { useNewUrlParser: true, useUnifiedTopology: true });

const db = mongoose.connection;

db.on("error", (err) => console.error(err));

db.once("open", (_) => {
  console.log("opened connection");
});

function generateUploadID(username, date, files) {
  return username + "-" + date;
}

function getUserUploads(username) {
  
}

function storeToDatabase(files, username, polys, date) {
  let imageNames = [];
  files.forEach((f) => imageNames.push(f.filename));

  const id = generateUploadID(username, date, files);

  let imageUpload = new ImageUpload({
    images: imageNames,
    data: Date,
    user: username,
    uploadId: id,
  });

  let polyArr = [];
  for (let i = 0; i < polys.length; ++i) {
    const path = polys[i];
    let latLngs = [];
    for (let j = 0; j < path.length; ++j) {
      latLngs.push([parseFloat(path[j].lat), parseFloat(path[j].lng)]);
    }
    polyArr.push(latLngs);
  }

  let polysUpload = new Polys({
    type: "Polygon",
    date: date,
    user: username,
    coordinates: polyArr,
    uploadId: id,
  });

  console.log("polys", polysUpload);

  imageUpload.save((err, upload) => {
    if (err) {
      console.log(err);
    } else {
      console.log(upload);
      polysUpload.save((err, upload) => {
        if (err) console.log(err);
        else console.log(upload);
      });
    }
  });
}

async function getAreaPolys() {
  let polysArr = [];
  await Polys.find({}, (err, res) => {
    if(err) {
      console.log(err);
      return;
    } else {
      console.log(res);
      for(let i = 0; i < res.length; ++i) {
        const polys = res[i];
        let polysForUpload = {date : polys.date, id : polys.uploadId, coordinates : []};
        console.log('polys: ', polys);
        for(let j = 0; j < polys.coordinates.length; ++j) {
          const currentpath = polys.coordinates[j];
          console.log('path: ', currentpath);
          let path = [];
          for(let k = 0; k < currentpath.length; ++k) {
            const latlng = currentpath[k];
            console.log(latlng);
            path.push({lat : latlng[0], lng : latlng[1]});
          }
          polysForUpload.coordinates.push(path);
        }
        polysArr.push(polysForUpload);
      }
    }
  });
  return polysArr;
}

module.exports = { storeToDatabase, getAreaPolys };
