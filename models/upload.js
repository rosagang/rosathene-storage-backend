const mongoose = require("mongoose");

const PointSchema = new mongoose.Schema({
  type: {
    type: String, // Don't do `{ location: { type: String } }`
    enum: ["Point"], // 'location.type' must be 'Point'
    required: true,
  },
  coordinate: {
    type: Number,
    required: true,
  },
});

const PolysSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: ["Polygon"],
    required: true,
  },
  coordinates: {
    type: [[[Number]]], // Array of arrays of arrays of numbers
    required: true,
  },
  date: Date,
  user: String,
  uploadId: String,
});

const ImageUploadSchema = new mongoose.Schema({
  images: [String],
  date: Date,
  user: String,
  uploadId: String,
});

const Point = mongoose.model("Point", PointSchema);
const Polys = mongoose.model("Polys", PolysSchema);
const ImageUpload = mongoose.model("ImageUpload", ImageUploadSchema);

module.exports = { ImageUpload, Polys };
